import java.io.File

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.io.Source

object CFLogger {

  val configdir: String = null

  def configPull(args: Array[String]): Map[String,String] = {
    // Pull in the user config if it exists.
    // If not, then handle that with user input requests.
    // If user-input flags are present, use of.
    // Check for config exists and load parameters from that.
    Map(
      "logingestionfolder" -> "logingestionfolder"
    )
  }

  def logIngester(conf: Map[String,String]): ListBuffer[List[List[String]]] = {
    // Import the logs
    val logsource = new File(conf("logingestionfolder"))
    var loglist: List[File] = null
    if (logsource.exists && logsource.isDirectory) {
      loglist = logsource.listFiles.filter(_.isFile).toList
      if (loglist.isEmpty) {
        println("No logs found at " + conf("logingestionfolder") + "! Exiting...")
        System.exit(1)
      }
    } else {
      println("That directory looks to be either nonexistent, or pointing at a file. Exiting...")
      System.exit(1)
    }

    println("Found " + loglist.length + " logs to parse. Now collecting data...")
    var iteration = 0
    val logdata: ListBuffer[List[List[String]]] = new ListBuffer()

    do {
      println("Populating logfile " + (iteration + 1) + "/" + loglist.length + "...")

      logdata += Source.fromFile(loglist(iteration))
        .getLines
        .drop(2) // Required to skip the version number and headings of the log file as served from CloudFront.
        .map(_.split("\t").toList)
        .toList

      iteration += 1
    } while (loglist.length > logdata.length)
    println("")
    logdata
  }

  def logReport(logdata: ListBuffer[List[List[String]]]): Unit = {
    // For every column across all logfiles, determine the most frequent occurence of each item.
    // For some this won't be too useful (i.e. date or time) but for some this will be quite handy.

    var columns: mutable.Map[Int,mutable.Map[String,Int]] = null

    // Nasty instantiation.
    columns = mutable.Map(0 -> mutable.Map("" -> 0))

    // Make a map for each column based on its Integer ID (translate this to column name before printing),
    // and have each entry set as the Key with a Value equal to its occurrence frequency.

    for (i: Int <- 0 to 23) {
      // For the column #(i)...

      // Instantiate that column's map entry.
      columns(i) = mutable.Map("" -> 0)
      for (j <- 0 until logdata.length) {
        // ...check each logfile for...
        for (k <- 0 until logdata(j).length) {
          // ...every entry, and increment the instance of the value contained in column i's field.

          // This variable is the actual text contained in this field.
          val thisEntry = logdata(j)(k)(i)

          val currentMap: mutable.Map[String,Int] = columns(i)

          //println(currentMap)

          // Check if the map has this key, if key is not empty...
          if (!thisEntry.isEmpty) {
            if (columns(i).keySet.contains(thisEntry)) {
              // println("Found: " + thisEntry + ", adding...")
              // Each time that is seen, increment the counter for that entry text.
              columns(i)(thisEntry) += 1
            } else {
              // println("Found: " + thisEntry + " is new key! Adding...")
              columns(i)(thisEntry) = 1
            }
          }
        }
      }
    }

    println("")

    for (i <- 0 until columns.size) {
      for ((key,value) <- columns(i)) if (value != 0) println("Key " + key + ": Seen " + value + " times.")
    }
  }

  def logHitRatio(logdata: ListBuffer[List[List[String]]]): Unit = {
    // Hit:Miss ratio.

    //noinspection VarCouldBeVal // This is to tell Intellij to stop telling me this could be a val when it can't be. :P
    var entryHitMiss: mutable.Map[String,Int] = {
      mutable.Map(
        "Hit" -> 0,
        "Miss" -> 0,
        "Error" -> 0,
        "Total" -> 0
      )
    }

    for (i <- 0 until logdata.length) {
      val keys = List("Hit", "Miss", "Error")
      for (j <- 0 until logdata(i).length) {
        val currentValue = logdata(i)(j)(13)
        if (keys.exists(currentValue.contains)) {
          entryHitMiss(currentValue) += 1
          entryHitMiss("Total") += 1
        }
      }
    }

    if (entryHitMiss("Total") <= 0) println("No Hits, Misses or Errors found in these logs! Are these CloudFront logs?")
    else {
      println("Hit ratio results for this set of logs:")
      println("HIT:   " + (entryHitMiss("Hit") / entryHitMiss("Total").asInstanceOf[Double]) * 100 + "% (" + entryHitMiss("Hit") + ":" + entryHitMiss("Total") + ")")
      println("MISS:  " + (entryHitMiss("Miss") / entryHitMiss("Total").asInstanceOf[Double]) * 100 + "% (" + entryHitMiss("Miss") + ":" + entryHitMiss("Total") + ")")
      println("ERROR: " + (entryHitMiss("Error") / entryHitMiss("Total").asInstanceOf[Double]) * 100 + "% (" + entryHitMiss("Error") + ":" + entryHitMiss("Total") + ")")
    }
  }

  def logPrinter(logdata: ListBuffer[List[List[String]]]): Unit = {
    // The stored data at logdata(logFileNumber)(logEntryNumber)(x) corresponds to the following:
    // 0: date
    // 1: time
    // 2: x-edge-location
    // 3: sc-bytes
    // 4: c-ip
    // 5: cs-method
    // 6: cs(Host)
    // 7: cs-uri-stem
    // 8: sc-status
    // 9: cs(Referer)
    // 10: cs(User-Agent)
    // 11: cs-uri-query
    // 12: cs(Cookie)
    // 13: x-edge-result-type
    // 14: x-edge-request-id
    // 15: x-host-header
    // 16: cs-protocol
    // 17: cs-bytes
    // 18: time-taken
    // 19: x-forwarded-for
    // 20: ssl-protocol
    // 21: ssl-cipher
    // 22: x-edge-response-result-type
    // 23: cs-protocol-version

    println("-- ALL REQUESTED ITEMS --")
    // Print the 7th List item from each row.

    println("Logfiles Found: " + logdata.length)

    for (i <- 0 until logdata.length) {
      println("Logfile " + i + " with " + logdata(i).length + " entries.")
      for (j <- 0 until logdata(i).length) {
        println(logdata(i)(j))
      }
      println("")
    }

  }

  def main(args: Array[String]): Unit = {
    val conf = configPull(args)
    val logdata = logIngester(conf)
    logReport(logdata)
  }
}
