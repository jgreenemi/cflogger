# README #

CFLogger for ingesting AWS CloudFront access logs and see top requested and top requester information.

### Roadmap ###

#### Tasks of Function ####

[ ] Sort results as most popular entries to least.

[ ] Create Map of columnnames for result printing customization, ease of understanding after results are printed, etc.

[ ] Make for usable related data about entries. (Of the top requested items, who is requesting them most? Or vice versa?)

[x] Import the logs into a ListBuffer of Lists (each ListBuffer item is a row, and each row is a List of values).

[x] Read log files from a predetermined directory.

#### Tasks of Optimization ####

[ ] Print as a webpage or other visual table.

[ ] Flatten the initial loglist so we're not using a Map of Maps. User doesn't care which logs were in what logfiles when using this tool.

[ ] Can be expanded out from here to allow custom filtering and such, but for now I'll just keep it to Top Requested and Top Requesters (may do IP address, may do edge locations (Top Servers?)).

[ ] User-provided IAM user to read content from an S3 bucket to automatically pull down logs upon application launch, so they also don't have to download them.

[ ] Unpack all .gz archives before import, so the user doesn't have to.

